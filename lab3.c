#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <math.h>

int n1,n2,n3,n4;
int x,y;
const double pi=cos(-1);

typedef double type;
void testSimple(type *A,type *B) {
		for (int i=0;i<n1;++i) {
			for (int j=0;j<n2;++j) {
				A[i*n2+j]=0;
				if (i==j) A[i*n2+j]=1;
			}
		}
		for (int i=0;i<n3;++i) {
			for (int j=0;j<n4;++j) {
				B[i*n4+j]=0;
				if (i==j) B[i*n4+j]=1;
			}
		}
		return;
}
void testDigits(type *A,type *B) {
		char s[100]="123456789123456789121121123456789123456789"; //7*6
		char s1[100]="121121333333787878"; //6*3
		if (strlen(s)!=n1*n2) {
			printf("Error in Digits 1\n");
		}
		if (strlen(s1)!=n3*n4) {
			printf("Error in Digits 2\n");
		}
		for (int i=0;i<n1;++i) {
			for (int j=0;j<n2;++j) {
				A[i*n2+j]=s[i*n2+j]-'0';
			}
		}
		for (int i=0;i<n3;++i) {
			for (int j=0;j<n4;++j) {
				B[i*n4+j]=s1[i*n4+j]-'0';
			}
		}
		/*
		107.000000 109.000000 107.000000 
		83.000000 97.000000 83.000000 
		176.000000 184.000000 176.000000 
		31.000000 35.000000 31.000000 
		107.000000 109.000000 107.000000 
		83.000000 97.000000 83.000000 
		176.000000 184.000000 176.000000 
		*/
		return;
}
void testTest (type *A,type *B) {
	for (int i=0;i<n1;++i) {
		for (int j=0;j<n2;++j) {
			A[i*n2+j]=i*i+j*j;
			if (i==j) A[i*n2+j]=145.63;
			if ((i+j)%3==2) A[i*n2+j]=-5;
			if ((i+j)%11==7) A[i*n2+j]=259/375;
			if ((i+j)%35==7) A[i*n2+j]=-7;
		}
	}
	for (int i=0;i<n3;++i) {
		for (int j=0;j<n4;++j) {
			B[i*n4+j]=sin((2*pi*i)/n2);;
			if (i==j) B[j*n3+i]=i;
			if ((i+j)%3==2) B[i*n4+j]=35;
			if ((i+j)%11==1) B[i*n4+j]=1259/624;
			if ((i+j)%35==3) B[i*n4+j]=-73;
		}
	}
}
void writeM(type *A,type *B) {
		for (int i=0;i<n1;++i) {
			for (int j=0;j<n2;++j) {
				printf("%f ",A[i*n2+j]);
			}
			printf("\n");
		}
		printf("\n");
		for (int i=0;i<n3;++i) {
			for (int j=0;j<n4;++j) {
				printf("%f ",B[i*n4+j]);
			}
			printf("\n");
		}
		printf("\n");
}
int getCntInThisProcessA (int I, int length,int N,int M) {
	I=I/y;
	if (I<N%length) {
		return (N/length+1)*M;
	}
	return (N/length)*M;
}
int getcntStlInThisProcessB (int I, int length,int N,int M) {
	I=I%y;
	if (I<N%length) {
		return (N/length+1);
	}
	return (N/length);
}
int getPlaceInMatrixThisProcessA (int I, int length,int N,int M) {
	I=I/y;
	if (I<N%length) {
		return (N/length+1)*I*M;
	}
	return (N-(N/length)*(length-I))*M;	
}
int getPlaceInMatrixThisProcessB (int I, int length,int N,int M) {
	I=I%y;
	if (I<N%length) {
		return (N/length+1)*I;
	}
	return (N-(N/length)*(length-I));	
}
void fillArraysForV (int *cntInProcess,int *placeInMatrix,int cntInThisProcess,int placeInMatrixThisProcess,int I,MPI_Comm comm) {
	MPI_Allgather(&cntInThisProcess,1,MPI_INT,cntInProcess,1,MPI_INT,comm);
	MPI_Allgather(&placeInMatrixThisProcess,1,MPI_INT,placeInMatrix,1,MPI_INT,comm);
}
void multypl (type *A,type *B,type *R,int cntInThisProcessA,int cntStlInThisProcessB,int I) {
	int k=0;
	cntInThisProcessA/=n2;
	for (int i=0;i<cntInThisProcessA;++i) {
		for (int j=0;j<cntStlInThisProcessB;++j) {
			R[k]=0;
			for (int t=0;t<n2;++t) {
				R[k]+=(A[(i*n2+t)]*B[(j*n3+t)]);
			}
			//R[k]=I;
			++k;
		}
		if(I%y>=n4%y && n4%y) {
			//R[k]=I;
			++k;
			//printf("%d \n",I);
		} 
	}
}

int main (int argc,char *argv[]) {
	int cntPr,I;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&I);
	n2=n3=3000;
	n1=n2-2;
	n4=n3+2;
	int dims[2]={0,0};
	int periods[2]={0,0};
	int reorder=0;
	MPI_Comm comm2d;
	
	MPI_Comm_size(MPI_COMM_WORLD,&cntPr);

	MPI_Dims_create(cntPr,2,dims);
	
	x=dims[0];y=dims[1];

//printf("x=%d y=%d\n",x,y);
	MPI_Cart_create(MPI_COMM_WORLD,2,dims,periods,reorder,&comm2d);
	
	int lineIsHere[2];
	lineIsHere[0]=0;
	lineIsHere[1]=1;
	MPI_Comm commStr;
	MPI_Cart_sub(comm2d,lineIsHere,&commStr);

	lineIsHere[0]=1;
	lineIsHere[1]=0;
	MPI_Comm commStl;
	MPI_Cart_sub(comm2d,lineIsHere,&commStl);
	
	/*MPI_Comm commStr;
	int myStr=I/y;
	MPI_Comm_split(MPI_COMM_WORLD,myStr,I,&commStr);

	MPI_Comm commStl;
	int myStl=I%y;
	MPI_Comm_split(MPI_COMM_WORLD,myStl,I,&commStl);//*/

	int cntInThisProcessA=getCntInThisProcessA(I,x,n1,n2);
	int cntStlInThisProcessB=getcntStlInThisProcessB(I,y,n4,n3);
	int placeInMatrixThisProcessA=getPlaceInMatrixThisProcessA(I,x,n1,n2);
	int placeInMatrixThisProcessB=getPlaceInMatrixThisProcessB(I,y,n4,n3);
//	printf("%d: %d %d\n",I,cntInThisProcessA,cntStlInThisProcessB);
	int *cntInProcessA=malloc(sizeof(int)*x);//
	int *cntStlInProcessB=malloc(sizeof(int)*y);//
	int *placeInMatrixA=malloc(sizeof(int)*x);//
	int *placeInMatrixB=malloc(sizeof(int)*y);//
	fillArraysForV (cntInProcessA,placeInMatrixA,cntInThisProcessA,placeInMatrixThisProcessA,I,commStl);//
	fillArraysForV (cntStlInProcessB,placeInMatrixB,cntStlInThisProcessB,placeInMatrixThisProcessB,I,commStr);//

	double *myA=malloc(sizeof(type)*cntInThisProcessA);
	double *myB=malloc(sizeof(type)*cntStlInThisProcessB*n3);
	type *A=NULL;
	type *B=NULL;
	
	if (!I) {
		/*for (int i=0;i<x;++i) {
			printf("%d: %d %d\n",i,placeInMatrixB[i],cntStlInProcessB[i]);
		}//*/
		A=malloc(sizeof(type)*n1*n2);
		B=malloc(sizeof(type)*n3*n4);
		testTest(A,B);
	//	writeM(A,B);
	}


	double t1,t2;	
	t1 = MPI_Wtime();

	if (I%y==0) MPI_Scatterv(A, cntInProcessA, placeInMatrixA, MPI_DOUBLE, myA, cntInThisProcessA,MPI_DOUBLE, 0, commStl);//double
	/*if (I==9) for (int i=0;i<cntInThisProcessA;++i) {
		printf("%f ",myA[i]);
	}
//	if (!I) printf("\n\n");//*/

	MPI_Datatype mpiVector,mpiVectorM;
	MPI_Type_vector(n3,1,n4,MPI_DOUBLE,&mpiVector);
	MPI_Type_commit(&mpiVector);
	MPI_Type_create_resized(mpiVector, 0, sizeof(double), &mpiVectorM);
	MPI_Type_commit(&mpiVectorM); //Нужно, чтобы были нормальные границы между типами вектор. Можно просто использовать extent для mpiVector, но это небезопасно (вроде, ну я где-то прочитала так)
	if (I/y==0)	MPI_Scatterv(B, cntStlInProcessB, placeInMatrixB, mpiVectorM, myB, cntStlInThisProcessB*n3,MPI_DOUBLE, 0, commStr);//double
	MPI_Type_free(&mpiVector);
	MPI_Type_free(&mpiVectorM);
	/*if (I==1) for (int i=0;i<cntStlInThisProcessB*n3;++i) {
		printf("%f ",myB[i]);
	}
	if (!I) printf("\n\n");//*/

	MPI_Bcast(myA,cntInThisProcessA,MPI_DOUBLE,0,commStr);
	/*if (I==11) for (int i=0;i<cntInThisProcessA;++i) {
		printf("%f ",myA[i]);
	}
	if (I==0) printf("\n\n");//*/

	MPI_Bcast(myB,cntStlInThisProcessB*n3,MPI_DOUBLE,0,commStl);
/*	if (I==2) for (int i=0;i<cntStlInThisProcessB*n3;++i) {
		printf("%f ",myB[i]);
	}
	if (I==1) printf("\n\n");//*/
	int sizeR=(cntInProcessA[0]/n2)*(cntStlInProcessB[0]);
	type *myR=malloc(sizeof(type)*sizeR);
	multypl(myA,myB,myR,cntInThisProcessA,cntStlInThisProcessB,I);
	/*if (I==1) for (int i=0;i<cntStlInThisProcessB*cntInThisProcessA/n2;++i) {
		printf("%f ",myR[i]);
	}
	if (I==0) printf("\n");//*/
	type *R=NULL;
	if (!I) {
		R=malloc(sizeof(type)*(n1+1)*(n4+1));
	}

	MPI_Datatype mpiMatrix,mpiMatrixM;
	MPI_Type_vector(cntInProcessA[0]/n2,cntStlInProcessB[0],n4,MPI_DOUBLE,&mpiMatrix);
	MPI_Type_commit(&mpiMatrix);
	MPI_Type_create_resized(mpiMatrix, 0, sizeof(double), &mpiMatrixM);
	MPI_Type_commit(&mpiMatrixM);
	free(placeInMatrixB);
	free(cntStlInProcessB);
	placeInMatrixB=malloc(sizeof(type)*cntPr);
	cntStlInProcessB=malloc(sizeof(type)*cntPr);
//	cntStlInThisProcessB=1;
	placeInMatrixThisProcessB+=((placeInMatrixThisProcessA/n2)*n4);
//	printf("%d: %d %d\n",I,placeInMatrixThisProcessA,placeInMatrixThisProcessB);
	fillArraysForV (cntStlInProcessB,placeInMatrixB,cntStlInThisProcessB,placeInMatrixThisProcessB,I,MPI_COMM_WORLD);//
//	int maxStr=n1/x+(1&(n1%x));;
//	printf("%d\n",maxStr);
	//for (int i=maxStr;i>=0;--i) {*/
/*	if (!I)	for (int i=0;i<cntPr;++i) {
			printf("%d: %d %d\n",i,placeInMatrixB[i],cntStlInProcessB[i]);
		}//*/
	//	MPI_Gatherv(myR,sizeR,MPI_DOUBLE,R,cntStlInProcessB,placeInMatrixB,mpiMatrixM,0,MPI_COMM_WORLD);
//	}
	if (I) {
		MPI_Send(myR,sizeR,MPI_DOUBLE,0,123,MPI_COMM_WORLD);
	} else {
		for (int i=0;i<x;++i) {
			for (int j=1;j<y;++j) {
				//printf("%d\n",i*y+j);
				MPI_Recv(R+placeInMatrixB[i*y+j],1,mpiMatrixM,i*y+j,123,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			}
		}
		for (int i=1;i<x;++i) {
			MPI_Recv(R+placeInMatrixB[i*y],1,mpiMatrixM,i*y,123,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		}
		cntInThisProcessA/=n2;
		for (int i=0;i<cntInThisProcessA;++i) {
			for (int j=0;j<cntStlInThisProcessB;++j) {
				R[i*n4+j]=myR[i*cntStlInThisProcessB+j];
			}
		}
	}
	MPI_Type_free(&mpiMatrix);
	MPI_Type_free(&mpiMatrixM);
	t2 = MPI_Wtime();
	t1=t2-t1;
	MPI_Allreduce(&t1,&t2,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);

	if (!I) {
		printf("Time,%f\n",t2);
	}
	
	/*if (!I) for (int i=0;i<n1;++i) {
		for (int j=0;j<n4;++j) {
		//	if (i==0 && j==0) printf("%f \n",*(myR+(0*cntStlInThisProcessB)%sizeR+1));
			printf("%f ",R[i*n4+j]);
		}
		printf("\n");
	}//*/
/*	if (I>=n1%x) {
		cntStlInThisProcessB=0;
		placeInMatrixThisProcessB=0;
	}
	fillArraysForV (cntStlInProcessB,placeInMatrixB,cntStlInThisProcessB,placeInMatrixThisProcessB,I,commStr);//
	//*/

/*	MPI_Comm commWithDop;
	int d=I<n1%x;
	MPI_Comm_split(MPI_COMM_WORLD,myStl,I,&commWithDop);*/

	/*if (d)*/ //MPI_Gatherv(myR+(maxStrWithoutOne*cntStlInThisProcessB)%sizeR,cntStlInThisProcessB,MPI_DOUBLE,R+maxStrWithoutOne*n4,cntStlInProcessB,placeInMatrixB,MPI_DOUBLE,0,MPI_COMM_WORLD);
	/*if (!I) for (int i=0;i<n1;++i) {
		for (int j=0;j<n4;++j) {
		//	if (i==0 && j==0) printf("%f \n",*(myR+(0*cntStlInThisProcessB)%sizeR+1));
			printf("%f ",R[i*n4+j]);
		}
		printf("\n");
	}*/
	if (!A) free(A);
	if (!B) free(B);
	if (!R) free(R);
	free(myR);
	if (!placeInMatrixA) free(placeInMatrixA);
	if (!placeInMatrixB) free(placeInMatrixB);
	if (!cntInProcessA)free(cntInProcessA);
	if (!cntStlInProcessB)free(cntStlInProcessB);
	if (!myA) free(myA);
	if (!myB) free(myB);
	MPI_Finalize();
	return 0;
}