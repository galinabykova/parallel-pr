#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>

const int Nx=55,Ny=55;
int N;
double eps=0.00001;

void matrixOnVector (double *M,double *V,double *R,int cntStrInThisProcess,int placeInMatrixThisProcess,int *cntStrInProcess,int *placeInMatrix,int cntPr,int I) {//
	double *T=malloc(sizeof(double)*cntStrInThisProcess);
	for (int i=0;i<cntStrInThisProcess;++i) {
		T[i]=0;
		for (int j=0;j<N;++j) {
			T[i]+=(( *(M+i*N+j) ) * ( *(V+j) ));
		}
	}
	MPI_Allgatherv(T,cntStrInThisProcess,MPI_DOUBLE,R,cntStrInProcess,placeInMatrix,MPI_DOUBLE,MPI_COMM_WORLD);
	free(T);
}

void vectorsLin (double *A,double a,double *B,double b,double *R,int cntStrInThisProcess) {//
	for (int i=0;i<N;++i) {
		R[i]=a*A[i]+b*B[i];
	}
}

double scal (double *A,double *B,int cntStrInThisProcess) {//
	double rez=0;
	for (int i=0;i<N;++i) {
		rez+=A[i]*B[i];
	}
	return rez;
}

double len (double *A,int cntStrInThisProcess) {
	return sqrt(scal(A,A,cntStrInThisProcess));
}

void testSimple (double* A,double *B,int cntStrInThisProcess,int I,int placeInMatrixThisProcess,int *cntStrInProcess,int *placeInMatrix,int cntPr) { //
	for (int i=0;i<cntStrInThisProcess;++i) {
		for (int j=0;j<N;++j) {
			if ((i+placeInMatrixThisProcess)==j) {
				*(A+i*N+j)=1;
			} else {
				*(A+i*N+j)=0;
			}
		}
	}
	for (int i=0;i<N;++i) { //
		B[i]=1;
	}
}

void testDigits (double* A,double *B,int cntStrInThisProcess,int I,int placeInMatrixThisProcess,int *cntStrInProcess,int *placeInMatrix,int cntPr) { //
	char s[1000]="123456789";
	for (int i=0;i<cntStrInThisProcess;++i) {
		for (int j=0;j<N;++j) {
			*(A+i*N+j)=s[(i+placeInMatrixThisProcess)*N+j]-'0';
		}
	}
	char s1[1000]="397";
	for (int i=0;i<N;++i) { //
		B[i]=s1[i]-'0';
	}
}

void testOne (double* A,double *B,int cntStrInThisProcess,int I,int placeInMatrixThisProcess,int *cntStrInProcess,int *placeInMatrix,int cntPr) { //
	for (int i=0;i<cntStrInThisProcess;++i) {
		for (int j=0;j<N;++j) {
			if ((i+placeInMatrixThisProcess)==j) {
				*(A+i*N+j)=2;
			} else {
				*(A+i*N+j)=1;
			}
		}
	}
	for (int i=0;i<N;++i) { //
		B[i]=N+1;
	}
}

void testTwo (double* A,double *B,int cntStrInThisProcess,int I,int placeInMatrixThisProcess,int *cntStrInProcess,int *placeInMatrix,int cntPr) { //
	for (int i=0;i<cntStrInThisProcess;++i) {
		for (int j=0;j<N;++j) {
			if ((i+placeInMatrixThisProcess)==j) {
				*(A+i*N+j)=2;
			} else {
				*(A+i*N+j)=1;
			}
		}
	}
	double U[N];
	double pi=acos(-1);
	for (int i=0;i<N;++i) { //
		U[i]=sin((2*pi*i)/N);
	}
	matrixOnVector(A,U,B,cntStrInThisProcess,placeInMatrixThisProcess,cntStrInProcess,placeInMatrix,cntPr,I);
}

void testThree (double* A,double *B,int cntStrInThisProcess,int I,int placeInMatrixThisProcess,int *cntStrInProcess,int *placeInMatrix,int cntPr) { //
	for (int i=0;i<cntStrInThisProcess;++i) {
		for (int j=0;j<N;++j) {
			*(A+i*N+j)=0;
		}
	}
		for (int i=0;i<cntStrInThisProcess;++i) {
			for (int j=0;j<N;++j) {
				int sdvi=i+placeInMatrixThisProcess;
				if (sdvi==j) {
					*(A+i*N+j)=-4;
				} else if (sdvi-j==1 && j%Nx!=Nx-1) {
					*(A+i*N+j)=1;
				} else if (sdvi-j==-1 && j%Nx) {
					*(A+i*N+j)=1;
				} else if (sdvi-j==Nx) {
					*(A+i*N+j)=1;
				} else if (sdvi-j==-Nx) {
					*(A+i*N+j)=1;
				} else {
					*(A+i*N+j)=0;
				}
			}
		}
	for (int i=0;i<N;++i) { //
		B[i]=0;
	}
	if (Ny>3) B[3*Nx]=25;
	if (Ny>2) B[2*Nx]=-25;
	if (Ny>1) B[Nx]=25;
	B[0]=-25;
}

void writeM (double* A,const char *name,char *file,int cntStrInThisProcess,int cntPr,int I) {  
	int t=0,k=0;
	FILE *f;
	if (!I) {
		f=fopen(file,"a+");
		fprintf(f,"%s:\n",name);
	} else {
		MPI_Recv(&t,1,MPI_INT,(cntPr+I-1)%cntPr,123,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		f=fopen(file,"a+");
	}
	
	for (int i=0;i<cntStrInThisProcess;++i) {
		for (int j=0;j<N;++j) {
			fprintf(f,"%.03f ",*(A+i*N+j));
		}
		fprintf(f,"\n");
	}
	fclose(f);
	if (cntPr>1) {
		MPI_Send(&k,1,MPI_INT,(I+1)%cntPr,123,MPI_COMM_WORLD);
		if (!I) {
			MPI_Recv(&t,1,MPI_INT,(cntPr+I-1)%cntPr,123,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		}
	}
}

void writeV (double *B,const char *name,char *file,int cntStrInThisProcess,int cntPr,int I) { // 
	if (!I) {
		FILE *f;
		f=fopen(file,"a+");
		fprintf(f,"%s:\n",name);
		for (int i=0;i<N;++i){
			fprintf(f,"%.03f ",B[i]);
		}
		fprintf(f,"\n\n");
		fclose(f);
	}
}

int getCntStrInThisProcess (int I, int cntPr) {
	if (I<N%cntPr) {
		return N/cntPr+1;
	}
	return N/cntPr;
}

int getPlaceInMatrixThisProcess (int I, int cntPr) {
	if (I<N%cntPr) {
		return (N/cntPr+1)*I;
	}
	return N-(N/cntPr)*(cntPr-I);	
}

void fillArraysForAllgatherv (int *cntStrInProcess,int *placeInMatrix,int cntPr,int cntStrInThisProcess,int placeInMatrixThisProcess,int I) {
	MPI_Allgather(&cntStrInThisProcess,1,MPI_INT,cntStrInProcess,1,MPI_INT,MPI_COMM_WORLD);
	MPI_Allgather(&placeInMatrixThisProcess,1,MPI_INT,placeInMatrix,1,MPI_INT,MPI_COMM_WORLD);
}
int main (int argc,char *argv[]) {
	int cntPr,I;
	N=Nx*Ny;
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&cntPr);
	MPI_Comm_rank(MPI_COMM_WORLD,&I);
	int cntStrInThisProcess=getCntStrInThisProcess(I,cntPr);
	int placeInMatrixThisProcess=getPlaceInMatrixThisProcess(I,cntPr);
	double *A=malloc(sizeof(double)*cntStrInThisProcess*N);
	int *cntStrInProcess=malloc(sizeof(int)*cntPr);//
	int *placeInMatrix=malloc(sizeof(int)*cntPr);//
	double B[N]; //
	fillArraysForAllgatherv (cntStrInProcess,placeInMatrix,cntPr,cntStrInThisProcess,placeInMatrixThisProcess,I);//
	testThree(A,B,cntStrInThisProcess,I,placeInMatrixThisProcess,cntStrInProcess,placeInMatrix,cntPr);
	//matrixOnVector(A,B,Y,cntStrInThisProcess,placeInMatrixThisProcess,cntStrInProcess,placeInMatrix);
//	writeM(A,"A","start.txt",cntStrInThisProcess,cntPr,I);
//	writeV(B,"B","start.txt",cntStrInThisProcess,cntPr,I);

	double X[N],Y[N],AY[N];
	for (int i=0;i<N;++i) {
		X[i]=0;
	}
	double lenB=len(B,cntStrInThisProcess);
	int i=0;

	double t1,t2;	
	t1 = MPI_Wtime();

	while (1) {
		matrixOnVector(A,X,Y,cntStrInThisProcess,placeInMatrixThisProcess,cntStrInProcess,placeInMatrix,cntPr,I);
		vectorsLin(Y,1,B,-1,Y,cntStrInThisProcess);
	//	writeV(Y,"Y1","start.txt",cntStrInThisProcess,cntPr,I);

		double lenY=len(Y,cntStrInThisProcess);
		if (lenY/lenB<eps) {
			break;
		}

		matrixOnVector(A,Y,AY,cntStrInThisProcess,placeInMatrixThisProcess,cntStrInProcess,placeInMatrix,cntPr,I);	
	//	writeV(AY,"AY1","start.txt",cntStrInThisProcess,cntPr,I);	

		double r=scal(Y,AY,cntStrInThisProcess)/scal(AY,AY,cntStrInThisProcess);
		//printf("%f\n",r);

		vectorsLin(X,1,Y,-r,X,cntStrInThisProcess);
	//	writeV(X,"X","start.txt",cntStrInThisProcess,cntPr,I);
		++i;
	}

	if (!I) {
		printf("%d\n",i);
	}

	t2 = MPI_Wtime();
	t1=t2-t1;
	MPI_Allreduce(&t1,&t2,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);

	if (!I) {
		printf("Time,%f\n",t2);
	}

	free(placeInMatrix);
	free(cntStrInProcess);
	free(A);
	MPI_Finalize();
	return 0;
}
