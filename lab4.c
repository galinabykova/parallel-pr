#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <math.h>

const double x0=-1;
const double y0C=-1;
const double z0=-1;
const double x1=1;
const double y1C=1;
const double z1=1;
const double a=1;
const double eps=1e-8;

double k;
double p1_hX_2,p1_hY_2,p1_hZ_2;
double hX,hY,hZ;

int Nx,Ny,Nz;

double fi(double x,double y,double z) {return x*x+y*y+z*z;}

double fiAll(double x,double y,double z,double *p,double *dif) {
	if (x==x1 || y==y1C || z==z1 || x==x0 || y==y0C || z==z0) {
		return fi(x,y,z);
	}
	return 0;
}

double p(double x,double y,double z) {return 6-a*fi(x,y,z);}

int getCntStr (int I,int cntStrAll,int cntPr) {
	if (I<cntStrAll%cntPr) {
		return (cntStrAll+cntPr-1)/cntPr;
	}
	return cntStrAll/cntPr;
}

int getPlace (int I,int cntStrAll,int cntPr) {
	if (I<cntStrAll%cntPr) {
		return ((cntStrAll+cntPr-1)/cntPr)*I;
	}
	return cntStrAll-(cntStrAll/cntPr)*(cntPr-I);	
}

double compute (double x,double y,double z,double *myPlace,double *dif) {
	if (x==x1 || y==y1C || z==z1 || x==x0 || y==y0C || z==z0) {
		(*dif)=0;
		return *myPlace;
	}
	double compX=(*(myPlace-Ny)+(*(myPlace+Ny)))*p1_hX_2;
	double compY=(*(myPlace-1)+(*(myPlace+1)))*p1_hY_2;
	double compZ=(*(myPlace-Nx*Ny)+(*(myPlace+Nx*Ny)))*p1_hZ_2;
	double rez=k*(compX+compY+compZ-p(x,y,z));

	(*dif)=fabs(*(myPlace)-rez);
	return rez;
}

double step (double *myStart,double *myLastStart,double (*f) (double,double,double,double*,double*),int I,int cntPr,int cntStrZInThisProcess,int placeZInThisProcess,int Nx_Ny) {
	double tec,max=0;
	int cntStrZInThisProcessWithoutOne=cntStrZInThisProcess-1;
	for (int i=0;i<Nx;++i) {
		for (int j=0;j<Ny;++j) {
			*(myStart+0*Nx_Ny+i*Ny+j)=f(x0+i*hX,y0C+j*hY,(z0+0+placeZInThisProcess)*hZ,myLastStart+0*Nx*Ny+i*Ny+j,&tec);
			if (tec>max) max=tec;
			*(myStart+cntStrZInThisProcessWithoutOne*Nx_Ny+i*Ny+j)=f(x0+i*hX,y0C+j*hY,(z0+cntStrZInThisProcessWithoutOne+placeZInThisProcess)*hZ,myLastStart+cntStrZInThisProcessWithoutOne*Nx*Ny+i*Ny+j,&tec);
			if (tec>max) max=tec;
		}
	}

	MPI_Request reqSend0,reqRecv0;
	if (I!=cntPr-1) MPI_Irecv(myStart+cntStrZInThisProcess*Nx_Ny,Nx_Ny,MPI_DOUBLE,I+1,123,MPI_COMM_WORLD,&reqRecv0);
	if (I) MPI_Isend(myStart,Nx_Ny,MPI_DOUBLE,I-1,123,MPI_COMM_WORLD,&reqSend0);

	MPI_Request reqSend1,reqRecv1;
	if (I) MPI_Irecv(myStart-Nx_Ny,Nx_Ny,MPI_DOUBLE,I-1,123,MPI_COMM_WORLD,&reqRecv1);
	if (I!=cntPr-1) MPI_Isend(myStart+(cntStrZInThisProcess-1)*Nx_Ny,Nx_Ny,MPI_DOUBLE,I+1,123,MPI_COMM_WORLD,&reqSend1);

	for (int k=1;k<cntStrZInThisProcessWithoutOne;++k) {
		for (int i=0;i<Nx;++i) {
			for (int j=0;j<Ny;++j) {
				*(myStart+k*Nx_Ny+i*Ny+j)=f(x0+i*hX,y0C+j*hY,(z0+k+placeZInThisProcess)*hZ,myLastStart+k*Nx_Ny+i*Ny+j,&tec);
				if (tec>max) max=tec;
			}
		}
	}
	if (I) MPI_Wait(&reqSend0,MPI_STATUS_IGNORE); //нужно ли?
	if (I!=cntPr-1) MPI_Wait(&reqSend1,MPI_STATUS_IGNORE); //нужно ли?
	if (I!=cntPr-1) MPI_Wait(&reqRecv0,MPI_STATUS_IGNORE);
	if (I) MPI_Wait(&reqRecv1,MPI_STATUS_IGNORE);
	double ret;
	MPI_Allreduce(&max,&ret,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
	return ret;
}

int main (int argc,char *argv[]) {
	Nx=90;
	Ny=90;
	Nz=90;
	hX=(x1-x0)/(Nx-1);
	hY=(y1C-y0C)/(Ny-1);
	hZ=(z1-z0)/(Nz-1);
	p1_hX_2=1/(hX*hX);
	p1_hY_2=1/(hY*hY);
	p1_hZ_2=1/(hZ*hZ);

	MPI_Init(&argc,&argv);
	double t1,t2;	
	t1 = MPI_Wtime();

	int cntPr,I;
	MPI_Comm_rank(MPI_COMM_WORLD,&I);
	MPI_Comm_size(MPI_COMM_WORLD,&cntPr);

	int cntStrZInThisProcess=getCntStr(I,Nz,cntPr);
	int placeZInThisProcess=getPlace(I,Nz,cntPr);
	//printf("%d: %d\n",I,placeZInThisProcess);
	k=0.5/(p1_hX_2+p1_hY_2+p1_hZ_2+a);
	int Nx_Ny=Nx*Ny;
	double *myWorld=malloc(sizeof(double)*(cntStrZInThisProcess+2)*Nx_Ny);
	
	step (myWorld+Nx_Ny,NULL,fiAll,I,cntPr,cntStrZInThisProcess,placeZInThisProcess,Nx_Ny);

	/*if (I==2) {
	for (int k=0;k<cntStrZInThisProcess+2;++k) {
		for (int i=0;i<Nx;++i) {
			for (int j=0;j<Ny;++j) {
				printf("%f ",*(myWorld+k*Nx_Ny+i*Ny+j));
			}
			printf("\n");
		}
		printf("\n");
	}
	}
	//*/
	double *myNewWorld=malloc(sizeof(double)*(cntStrZInThisProcess+2)*Nx_Ny);
	double max=5;
	int cnt=0;
	while (max>=eps) {
		double *z=myWorld;
		myWorld=myNewWorld;
		myNewWorld=z;
		max=step (myWorld+Nx_Ny,myNewWorld+Nx_Ny,compute,I,cntPr,cntStrZInThisProcess,placeZInThisProcess,Nx_Ny);
		cnt++;
	}
	
	/*if (I==0) {
	for (int k=0;k<cntStrZInThisProcess+2;++k) {
		for (int i=0;i<Nx;++i) {
			for (int j=0;j<Ny;++j) {
				printf("%f ",*(myWorld+k*Nx_Ny+i*Ny+j));
			}
			printf("\n");
		}
		printf("\n");
	}
	}
	//*/

	t2 = MPI_Wtime();
	t1=t2-t1;
	MPI_Allreduce(&t1,&t2,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);

	if (!I) {
		printf("Time,%f\n",t2);
	}

	free(myNewWorld);
	free(myWorld);
	MPI_Finalize();
	return 0;
}