//omp_get_num_threads();

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <time.h>  

const int Nx=55,Ny=55;
int N;
double eps=0.00001;

void matrixOnVector (double *M,double *V,double *R) {//
	#pragma omp parallel for schedule(guided,omp_get_num_threads())
	for (int i=0;i<N;++i) {
		R[i]=0; 
		//printf("%d\n",omp_get_thread_num());
		for (int j=0;j<N;++j) {
			R[i]+=(( *(M+i*N+j) ) * ( *(V+j) ));
		}
	}
}

void vectorsLin (double *A,double a,double *B,double b,double *R) {//
	#pragma omp parallel for 
	for (int i=0;i<N;++i) {
		//printf("%d\n",omp_get_thread_num());
		R[i]=a*A[i]+b*B[i];
	}
}

double scal (double *A,double *B) {//
	double rez=0;
	#pragma omp parallel for reduction(+:rez)
	for (int i=0;i<N;++i) {
		//printf("%d\n",omp_get_thread_num());
		rez+=A[i]*B[i];
	}
	return rez;
}

double len (double *A) {
	return sqrt(scal(A,A));
}

void testSimple (double* A,double *B) { //?
	for (int i=0;i<N;++i) {
		for (int j=0;j<N;++j) {
			if (i==j) {
				*(A+i*N+j)=1;
			} else {
				*(A+i*N+j)=0;
			}
		}
	}
	for (int i=0;i<N;++i) { //
		B[i]=1;
	}
}

void testDigits (double* A,double *B) { //
	char s[1000]="123456789";
	for (int i=0;i<N;++i) {
		for (int j=0;j<N;++j) {
			*(A+i*N+j)=s[i*N+j]-'0';
		}
	}
	char s1[1000]="397";
	for (int i=0;i<N;++i) { //
		B[i]=s1[i]-'0';
	}
}

void testOne (double* A,double *B) { //
	for (int i=0;i<N;++i) {
		for (int j=0;j<N;++j) {
			if (i==j) {
				*(A+i*N+j)=2;
			} else {
				*(A+i*N+j)=1;
			}
		}
	}
	for (int i=0;i<N;++i) { //
		B[i]=N+1;
	}
}

void testTwo (double* A,double *B,int cntStrInThisProcess,int I,int placeInMatrixThisProcess,int *cntStrInProcess,int *placeInMatrix,int cntPr) { //
	for (int i=0;i<N;++i) {
		for (int j=0;j<N;++j) {
			if (i==j) {
				*(A+i*N+j)=2;
			} else {
				*(A+i*N+j)=1;
			}
		}
	}
	double U[N];
	double pi=acos(-1);
	for (int i=0;i<N;++i) { //
		U[i]=sin((2*pi*i)/N);
	}
	matrixOnVector(A,U,B);
}

void testThree (double* A,double *B) { //
	for (int i=0;i<N;++i) {
		for (int j=0;j<N;++j) {
			*(A+i*N+j)=0;
		}
	}
	for (int i=0;i<N;++i) {
		for (int j=0;j<N;++j) {
			if (i==j) {
				*(A+i*N+j)=-4;
			} else if (i-j==1 && j%Nx!=Nx-1) {
				*(A+i*N+j)=1;
			} else if (i-j==-1 && j%Nx) {
				*(A+i*N+j)=1;
			} else if (i-j==Nx) {
				*(A+i*N+j)=1;
			} else if (i-j==-Nx) {
				*(A+i*N+j)=1;
			} else {
				*(A+i*N+j)=0;
			}
		}
	}
	for (int i=0;i<N;++i) { //
		B[i]=0;
	}
	if (Ny>3) B[3*Nx]=25;
	if (Ny>2) B[2*Nx]=-25;
	if (Ny>1) B[Nx]=25;
	B[0]=-25;
}

void writeM (double* A,const char *name,char *file) {  
	int t=0,k=0;
	FILE *f;
	f=fopen(file,"a+");
	for (int i=0;i<N;++i) {
		for (int j=0;j<N;++j) {
			fprintf(f,"%.03f ",*(A+i*N+j));
		}
		fprintf(f,"\n");
	}
	fclose(f);
}

void writeV (double *B,const char *name,char *file) { // 
	FILE *f;
	f=fopen(file,"a+");
	fprintf(f,"%s:\n",name);
	for (int i=0;i<N;++i){
		fprintf(f,"%.03f ",B[i]);
	}
	fprintf(f,"\n\n");
	fclose(f);
}

int main (int argc,char *argv[]) {
	N=Nx*Ny;
	double *A=malloc(sizeof(double)*N*N);
	double X[N],Y[N],AY[N];
	double B[N]; 
	//MPI_Comm_size(MPI_COMM_WORLD,&cntPr);
	//MPI_Comm_rank(MPI_COMM_WORLD,&I);
	testThree(A,B);
	//matrixOnVector(A,B,Y);
	//writeM(A,"A","start.txt");
	//writeV(B,"B","start.txt");
	//writeV(Y,"Y","start.txt");
	for (int i=0;i<N;++i) {
		X[i]=0;
	}
	double lenB=len(B);
	int i=0;
	
	struct timespec start, end;
	clock_gettime(CLOCK_MONOTONIC_RAW, &start);
	//#pragma omp paralllel
	//{//
	while (1) {
		matrixOnVector(A,X,Y);
		vectorsLin(Y,1,B,-1,Y);
	//	writeV(Y,"Y1","start.txt",cntStrInThisProcess,cntPr,I);

		double lenY=len(Y);
		if (lenY/lenB<eps) {
			break;
		}

		matrixOnVector(A,Y,AY);	
	//	writeV(AY,"AY1","start.txt",cntStrInThisProcess,cntPr,I);	

		double r=scal(Y,AY)/scal(AY,AY);
		//printf("%f\n",r);

		vectorsLin(X,1,Y,-r,X);
	//	writeV(X,"X","start.txt",cntStrInThisProcess,cntPr,I);
		++i;
		//if (i%100==0) printf("%d\n",i);
	}
	//}
	printf("%d\n",i);
	clock_gettime(CLOCK_MONOTONIC_RAW, &end);
	printf("Time taken: %lf sec.\n",end.tv_sec-start.tv_sec+ 0.000000001*(end.tv_nsec-start.tv_nsec));
	free(A);
	return 0;
}
