#include <stdio.h>
#include <mpi.h>
#include <pthread.h>
#include <stdlib.h>
#include <math.h>

double globalRes=0;
int iterCounter=0;
int gate=5000;
int theFirstInThisProcess;
int theLastPlusOneInThisProcess;
pthread_mutex_t mutex;
int doSendRecv;
int devided;
void task (int number) {
	int cnt=abs(iterCounter+number*number*number+1);//????
	for (int i=0;i<cnt;++i) globalRes+=sqrt(i);
	//printf("%d complete! It computed %d times\n",number,cnt);
}
void * work (void *a) {
	int cnt=0;
	while (1) {
		while (theFirstInThisProcess<theLastPlusOneInThisProcess) {
			task(theFirstInThisProcess);
			pthread_mutex_lock(&mutex);//
			++theFirstInThisProcess;
			pthread_mutex_unlock(&mutex);
			++cnt;
		}
		if (!doSendRecv) {
			break;
		}
		/*if (I)*/ MPI_Send(&cnt,1,MPI_INT,0,1,MPI_COMM_WORLD);
		int realI;
		/*else */  MPI_Recv(&realI,1,MPI_INT,0,2,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		if (realI!=-2) {
			int mes=-1;
			MPI_Send(&mes,1,MPI_INT,realI,1,MPI_COMM_WORLD);
			int newLine[2];
			MPI_Recv(newLine,2,MPI_INT,realI,3,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			pthread_mutex_lock(&mutex);//
			theFirstInThisProcess=newLine[0];
			theLastPlusOneInThisProcess=newLine[1];
			pthread_mutex_unlock(&mutex);
		} else {
			break;
		}
	}
	return NULL;
}

int getTheFirstInThisProcessEq(int I,int cntPr,int N) {
	if (I<N%cntPr) {
		return (N/cntPr+1)*I;
	}
	return N-(N/cntPr)*(cntPr-I);		
}

int getCntStrInThisProcessEq (int I, int cntPr,int N) {
	if (I<N%cntPr) {
		return N/cntPr+1;
	}
	return N/cntPr;
}

int getTheFirstInThisProcessOne(int I,int cntPr,int N) {
	if (!I) return 0;	
	return N;	
}

int getCntStrInThisProcessOne (int I, int cntPr,int N) {
	if (!I) return N;
	return 0;
}

int getTheFirstInThisProcessMd(int I,int cntPr,int N) {
	return (2*2+2*(I-1))*(I)/2;	
}

int getCntStrInThisProcessMd (int I, int cntPr,int N) {
	if (I!=cntPr-1) {
		return 0+2*(I+1);
	}
	return N-(2*2+2*(I-1))*(I)/2;
}

typedef struct table {
	int *addr;
	int cntProc;
} table;

void initT (table *T,int cntPr) {
	T->addr=malloc(sizeof(int*)*cntPr);
	for (int i=0;i<cntPr;++i) {
		*(T->addr+i)=rand()%cntPr;
	}
	T->cntProc=cntPr;
}
int IBelieveIReturnTheBestProcess (table *T,int a) {
	int max=-1,maxi;
	int g=T->cntProc;
	*(T->addr+a)=-1;
	for (int i=0;i<g;++i) {
		if (*(T->addr+i)>max) {
			maxi=i;
			max=*(T->addr+i);
		}
		(*(T->addr+i))++;
	}
	*(T->addr+maxi)=0;
	return maxi;
}
void destroy (table *T) {free(T->addr);}

int main (int argc,char *argv[]) {
	if (argc<3) {
		printf("./a.out doSendRecv devided\n");
		return 0;
	}
	doSendRecv=atoi(argv[1]);
	devided=atoi(argv[2]);
	int I,cntPr;
	int provided;
	MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE,&provided);
	double t1,t2;	
	t1 = MPI_Wtime();

	MPI_Comm_rank(MPI_COMM_WORLD,&I);
	MPI_Comm_size(MPI_COMM_WORLD,&cntPr);
	if (!I && provided!=MPI_THREAD_MULTIPLE) {
		perror("ERROR PROVIDED");
		MPI_Finalize();
	}
	
	//sleep(1);
	if (pthread_mutex_init(&mutex,NULL)!=0) {
		perror("ERROR mutex_init");
		MPI_Finalize();
		return 0;
	}
	pthread_attr_t attr;
	pthread_t worker;
	while (iterCounter<10) {
		int N;
		int cntWas=0;
		if (!I) {
			N=272;//(iterCounter*1000)/*rand()*/%gate+100;
		}
		MPI_Bcast(&N,1,MPI_INT,0,MPI_COMM_WORLD);
		//printf("%d: N=%d\n",I,N);
		if (devided==0) {
			theFirstInThisProcess=getTheFirstInThisProcessEq(I,cntPr,N);
			theLastPlusOneInThisProcess=theFirstInThisProcess+getCntStrInThisProcessEq(I,cntPr,N);
		}
		if (devided==1) {
			theFirstInThisProcess=getTheFirstInThisProcessOne(I,cntPr,N);
			theLastPlusOneInThisProcess=theFirstInThisProcess+getCntStrInThisProcessOne(I,cntPr,N);
		}
		if (devided==2) {
			theFirstInThisProcess=getTheFirstInThisProcessMd(I,cntPr,N);
			theLastPlusOneInThisProcess=theFirstInThisProcess+getCntStrInThisProcessMd(I,cntPr,N);
		}
		//printf("%d: %d-%d\n",I,theFirstInThisProcess,theLastPlusOneInThisProcess-1);
		
		table t;
		initT(&t,cntPr);

		if (pthread_attr_init(&attr)!=0) {
		perror("ERROR ATTR_INIT");
		MPI_Finalize();
		return 0;
		}
		if (pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_JOINABLE)!=0) {
		perror("ERROR attr setdetachstate");
		MPI_Finalize();
		return 0;
		}
		if (pthread_create(&worker,&attr,work,NULL)!=0) {
			perror("ERROR thread_create(&worker,&attr...");
			if (pthread_attr_destroy(&attr)!=0) {
				perror("ERROR attr destroy");
			}
			MPI_Finalize();
			return 0;
		}	
		int cntEnd=0;
		if (doSendRecv) {
			if (!I) {
				while (cntEnd<cntPr) {
					int mes=1;
					MPI_Status status;
					MPI_Recv(&mes,1,MPI_INT,MPI_ANY_SOURCE,1,MPI_COMM_WORLD,&status);
					if (mes>=0) {//?
						cntWas+=mes;
						if (cntWas<N) {
							int rec=IBelieveIReturnTheBestProcess(&t,status.MPI_SOURCE);
							//printf("%d\n",rec);
							MPI_Send(&rec,1,MPI_INT,status.MPI_SOURCE,2,MPI_COMM_WORLD);
						} else {
							int rec=-2;
							++cntEnd;
							MPI_Send(&rec,1,MPI_INT,status.MPI_SOURCE,2,MPI_COMM_WORLD);
						}
					}
					if (mes==-1) {
						pthread_mutex_lock(&mutex);//
						int forAnother[2];
						forAnother[1]=theLastPlusOneInThisProcess;
						theLastPlusOneInThisProcess=(theFirstInThisProcess+theLastPlusOneInThisProcess)/2;
						forAnother[0]=theLastPlusOneInThisProcess;
						pthread_mutex_unlock(&mutex);
						MPI_Send(forAnother,2,MPI_INT,status.MPI_SOURCE,3,MPI_COMM_WORLD);
						//printf("%d helps %d with %d-%d\n",status.MPI_SOURCE,I,forAnother[0],forAnother[1]-1);
					}
				}
				int mes=-3;
				for (int i=1;i<cntPr;++i) {
					MPI_Send(&mes,1,MPI_INT,i,1,MPI_COMM_WORLD);
				}
			} else {
				int mes=1;
				while (1) {//
					MPI_Status status;
					MPI_Recv(&mes,1,MPI_INT,MPI_ANY_SOURCE,1,MPI_COMM_WORLD,&status);
					if (mes==-3) {
						break;
					}
					pthread_mutex_lock(&mutex);//
					int forAnother[2];
					forAnother[1]=theLastPlusOneInThisProcess;
					theLastPlusOneInThisProcess=(theFirstInThisProcess+theLastPlusOneInThisProcess)/2;
					forAnother[0]=theLastPlusOneInThisProcess;
					pthread_mutex_unlock(&mutex);
					MPI_Send(forAnother,2,MPI_INT,status.MPI_SOURCE,3,MPI_COMM_WORLD);
					//	printf("%d helps %d with %d-%d\n",status.MPI_SOURCE,I,forAnother[0],forAnother[1]-1);
				}
			}
		}

		void *a;
		if (pthread_join(worker,&a)!=0) {
		perror("ERROR pthread_join(worker...");
		MPI_Finalize();
		return 0;
		}
		destroy(&t);
		++iterCounter;
	}
	if (pthread_mutex_destroy(&mutex)!=0) {
		perror("ERROR mutex_destroy");
		MPI_Finalize();
		return 0;
	}
	if (pthread_attr_destroy(&attr)!=0) {
		perror("ERROR attr destroy");
		MPI_Finalize();
		return 0;

	MPI_Barrier(MPI_COMM_WORLD);
	}
//printf("%d: theFirstInThisProcess=%d\n",I,theFirstInThisProcess);
	t2 = MPI_Wtime();
	t1=t2-t1;
	MPI_Allreduce(&t1,&t2,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);

	if (!I) {
		printf("doSendRecv=%d, devided=%d, Time,%f %f\n",doSendRecv,devided,t2,globalRes);
	}

	MPI_Finalize();
}

